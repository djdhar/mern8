const express = require('express');
const router = express.Router();

const product_controller = require('../controllers/product.controller');

router.get('/test', product_controller.test);
router.post('/create', product_controller.product_create);
router.get('/get/:id', product_controller.product_details);
router.get('/get_all', product_controller.all_products);
router.get('/get_paginated', product_controller.get_paginated_products);
router.put('/:id/update', product_controller.product_update);
router.delete('/:id/delete', product_controller.product_delete);    
module.exports = router;